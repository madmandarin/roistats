import configparser
import requests
from datetime import datetime
from common.connection import Snapshots
from common.log import create_log


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('config.ini')
    lg = create_log(config['DEFAULT']['debug'])

    result = requests.get('https://www.roi.ru/api/petitions/poll.json')
    timestamp = datetime.utcnow()

    lg.info(f'Start parsing {len(result.json()["data"])} petitions')

    petition_list = []
    index = 0
    for petition in result.json()['data']:
        index += 1
        result = requests.get(f'https://www.roi.ru/api/petition/{petition["id"]}.json').json()
        lg.info(result['data']['vote'])
        petition_list.append({
            'roi_id': petition["id"],
            'timestamp': timestamp,
            'affirmative': result['data']['vote']['affirmative'],
            'negative': result['data']['vote']['negative'],
        })

        if index >= int(config['DEFAULT']['max_size_transaction']):
            Snapshots.insert_many(petition_list).execute()
            petition_list = []
            index = 0

    Snapshots.insert_many(petition_list).execute()
