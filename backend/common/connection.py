import configparser
from peewee import *
from playhouse.db_url import connect

config = configparser.ConfigParser()
config.read('config.ini')

db = connect(config['DEFAULT']['database'])


class Snapshots(Model):
    roi_id = IntegerField()
    timestamp = DateTimeField()
    affirmative = IntegerField()
    negative = IntegerField()

    class Meta:
        database = db
        indexes = (
            (('roi_id', 'timestamp'), True),
        )


db.create_tables([Snapshots], safe=True)
