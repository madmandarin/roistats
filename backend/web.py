#!flask/bin/python
import os
import configparser

from flask import Flask, jsonify
from flask_cors import CORS

from common.connection import Snapshots
from common.log import create_log

# Инициализация конфигов и логов
config = configparser.ConfigParser()
config.read('config.ini')
lg = create_log(config['DEFAULT']['debug'])


# Старт Flask
app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


# Новости
@app.route('/api/petition/<int:petition_id>', methods=['GET'])
def get_petitions(petition_id):

    return jsonify({'points': [{
        'timestamp': item.timestamp.timestamp(),
        'affirmative': item.affirmative,
        'negative': item.negative
    } for item in Snapshots.select().where(Snapshots.roi_id == petition_id).order_by(Snapshots.timestamp).execute()]})


# Время выполнений функций
@app.route('/api/test/', methods=['GET'])
def test():
    return jsonify({'test': True})


if __name__ == '__main__':
    app.run(debug=True)
