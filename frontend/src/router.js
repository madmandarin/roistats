import Vue from 'vue';
import Router from 'vue-router';
import Index from './views/Index.vue';
import Petition from './views/Petition.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Index,
    },
    {
      path: '/petition/:id',
      name: 'petition',
      component: Petition,
    },
  ],
});
